## Installation
	
### Get the Code

Either clone this repository or fork it on GitHub and clone your fork:


```
git clone https://burakatli@bitbucket.org/burakatli/editor.api.git
cd TextEditor.API
```

### App Server

Our backend application server is a ASP.net core application. Authentication, Authorization and REST API Security is implemented with JWT (JSON Web Token) is implemented using JWT .
Server repository must be downloaded from : 

```
https://bitbucket.org/burakatli/editor.api/src/master/
```

Entity Framework is used for creating database and all related database queries.
Therefore tables must be initialized before running server application.
To initialize database the following code should be executed via VS console.

```
Update-Database
```

* Browse to the application at [http://localhost:2000/]
